package ru.t1.skasabov.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showExit();

    void showErrorArgument();

    void showErrorCommand();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}
